﻿
#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream> 

struct Words {
	Words(int i, std::string Name) {
		Amount = i;
		Word = Name;
	}
public:
	int Amount;
	std::string Word;
};
void ReadWords(std::string filename, std::vector <Words>& WordsMasiv) {
	std::ifstream file(filename);
	if (!file) std::cout << "FileError" << std::endl; else {
	std::string line;
	while (!file.eof()) {
		file >> line;
		WordsMasiv.push_back(Words(0, line));
	}
	}
}
void FileRead(std::string &text, std::string filename) {
	std::ifstream file(filename);
	if (!file) std::cout << "FileError" << std::endl; else {
		std::string line;
		while (!file.eof()) {
			file >> line;
			text = text + ' ';
			text = text + line;
		}
	}
}
void TextPreapare(std::string& text) {
	for (int i = 0; i < text.size(); i++) {
		if (text[i] == '.' or text[i] == ',' or text[i] == ';' or text[i] == '—' or text[i] == ':') { text[i] = ' '; }
	}
}

void TextAnalize(std::string text, std::vector <Words> &GoodWords, std::vector <Words> &BadWords) {
	std::string line;
	std::string line2;
	std::stringstream Stext;
	Stext << text;
	int GoodWordAmount = GoodWords.size();
	int BadWordAmount = BadWords.size();
	while (!Stext.eof()) {
		line2 = line;
		Stext >> line;
		if (line == line2) {}
		else {
			for (int i = 0; i < GoodWordAmount; i++) {
				if (line == GoodWords[i].Word) { GoodWords[i].Amount++; i = i + GoodWordAmount; }
			}
			for (int i = 0; i < BadWordAmount; i++) {
				if (line == BadWords[i].Word) { BadWords[i].Amount++; i = i + BadWordAmount; }
			}
		}
	}
}
void WriteWordData(std::vector <Words> GoodWords, std::vector <Words> BadWords) {
	std::cout << "Positive words:" << std::endl << std::endl;
	for (int i = 0; i < GoodWords.size(); i++) {
		std::cout << GoodWords[i].Word << ':' << GoodWords[i].Amount << std::endl;
	}
	std::cout << std::endl;
	std::cout << "Negative words:" << std::endl << std::endl;
	for (int i = 0; i < BadWords.size(); i++) {
		std::cout << BadWords[i].Word << ':' << BadWords[i].Amount << std::endl;
	}
}

int main()
{
	setlocale(LC_ALL, "Russian");
	std::string text;
	std::string fileName;
	std::string input;
	std::vector <Words> GoodWords;
	std::vector <Words> BadWords;

	std::cout << "Analized document - National Security Strategy (02.2015).txt\n\nStatistics:\n";
	ReadWords("GoodWords.txt", GoodWords);
	ReadWords("BadWords.txt", BadWords);
	FileRead(text, "text.txt");
	TextPreapare(text);
	TextAnalize(text, GoodWords, BadWords);
	WriteWordData(GoodWords, BadWords);
}

